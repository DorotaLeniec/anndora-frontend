import React from 'react';
import {Link} from 'react-router-dom';
import {Col, Button, Table} from 'react-bootstrap';
import {connect} from 'react-redux';

import {fetchAllUsers, fetchLoggedUsers} from "./actions";


const mapStateToProps = state => ({
    token: state.currentUser.token,
    loggedUsers: state.users.loggedUsers,
    allUsers: state.users.allUsers
});

const mapDispatchToProps = dispatch => ({
    getAllUsers: (token) => dispatch(fetchAllUsers(token)),
    getLoggedUsers: (token) => dispatch(fetchLoggedUsers(token))

});


class Users extends React.Component {

    componentWillMount() {
        this.props.getAllUsers(this.props.token)
    }

    render() {
        return (
            <div>
                <Col xs={6} xsOffset={3} className="homeView">
                <style>
                    @import url('https://fonts.googleapis.com/css?family=Saira');
                </style>
                <h1 className="main-heading"> Users View </h1>
                {this.props.allUsers !== null ? (
                    <div>
                        <Table  bordered condensed hover>
                            <thead>
                            <tr>
                                <th>Zdjecie</th>
                                <th>Username</th>
                                <th>Płeć</th>
                            </tr>
                            </thead>

                            <tbody>

                            {this.props.allUsers.map(user => (
                                <tr>
                                    <th>
                                        <div className="user-photo">{user.username.substring(0,1).toUpperCase()}
                                        </div>
                                    </th>
                                    <th>{user.username}</th>
                                    <th>{user.sex}</th>
                                </tr>
                                )
                            )}
                            </tbody>
                        </Table>

                    </div>
                ):""}
                </Col>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users)