

const initialState = {
    pending:false,
    error:false,
    loggedUsers:null,
    allUsers:null
}


export default (state=initialState, action ) => {
    switch (action.type){
        case 'FETCH_LOGGED_USERS_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'FETCH_LOGGED_USERS_SUCCESS':
            return {
                ...state,
                pending:false,
                loggedUsers:action.loggedUsers
            };
        case 'FETCH_ALL_USERS_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'FETCH_ALL_USERS_SUCCESS':
            return {
                ...state,
                pending:false,
                allUsers:action.allUsers
            };
        case 'DELETE_USER_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'DELETE_USER_SUCCESS':
            return {
                ...state,
                pending:false
            };
        default:
            return state
    }
}