export const fetchAllUsers = (token) => {
    return dispatch => {
        dispatch({type: "FETCH_ALL_USERS_BEGIN"});

        fetch('http://localhost:8083/user/all', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key': token

            }
        }).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    response.json().then(
                        response => dispatch({type: 'FETCH_ALL_USERS_SUCCESS', allUsers: response})
                    )
                }
                else {
                    console.log('Blad ' + response)
                }
            }
        )

    }
};

export const fetchLoggedUsers = (token) => {
    return dispatch => {
        dispatch({type: "FETCH_LOGGED_USERS_BEGIN"});

        fetch('http://localhost:8083/user/all/logged', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key': token

            }
        }).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    response.json().then(
                        response => dispatch({type: 'FETCH_LOGGED_USERS_SUCCESS', loggedUsers: response})
                    )
                }
                else {
                    console.log('Blad ' + response)
                }
            }
        )

    }
};

export const deleteUser = (username, token) => {
    return dispatch => {
        dispatch({type: "DELETE_USER_BEGIN"});

        fetch('http://localhost:8083/user?username=' + username, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                "Accept": "*/*",
                'api_key': token

            }
        }).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 204) {
                    dispatch({type: 'DELETE_USER_SUCCESS'});
                    dispatch(fetchAllUsers(token))
                }
                else {
                    console.log('Blad ' + response)
                }
            }
        )

    }
};