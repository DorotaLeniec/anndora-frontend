import React from 'react';
import store from './store'
import {Provider} from 'react-redux'
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap-theme.css'

import Home from './HomeView/HomeView'
import Register from './RegisterView/RegisterView'
import Login from './LoginView/LoginView'
import SentMessage from './MessageView/SendMessageView'
import Admin from './AdminView/AdminView'
import Profile from './ProfilView/ProfilView'
import Navigation from './HomeView/Navigation'
import MessageBox from './MessageView/MessageBoxView'
import Message from './MessageView/MessageView'
import Success from './MessageView/SentSuccessView'
import Users from './UsersView/UsersView'

import './index.css'

export default () => (
    <div>
        <Provider store={store}>
            <BrowserRouter>
                <div>
                    <header>
                        <Navigation/>
                    </header>

                    <Switch>
                        <Route path={"/"} exact component={Home}/>
                        <Route path={"/rejestracja"} exact component={Register}/>
                        <Route path={"/logowanie"} exact component={Login}/>
                        <Route path={"/send"} exact component={SentMessage}/>
                        <Route path={"/admin"} exact component={Admin}/>
                        <Route path={"/profil"} exact component={Profile}/>
                        <Route path={"/inbox"} exact component={MessageBox}/>
                        <Route path={"/message/:id"} exact component={Message}/>
                        <Route path={"/send/success"} exact component={Success}/>
                        <Route path={"/users"} exact component={Users}/>
                    </Switch>
                </div>
            </BrowserRouter>
        </Provider>
    </div>
);