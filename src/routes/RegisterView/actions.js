
export const registerAction = (username, password,sex) => {
    return (dispatch) => {
        dispatch({type: 'REGISTER_BEGIN'});

        fetch('http://localhost:8083/user/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            },
            body: JSON.stringify({
                'username': username,
                'password': password,
                'sex': sex
            })
        }).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    dispatch({type: 'REGISTER_SUCCESS'});
                    document.getElementById('register-box').style.display = "block";
                }
                else {
                    let errorMessage = document.getElementById('register-error');
                    if (responseStatus === 403) {
                        errorMessage.innerText = 'Username jest zajęty'
                        document.getElementById('reUsername').style.borderColor = 'red';
                    } else if (responseStatus === 400) {
                        errorMessage.innerText = 'Podany użytkownik lub hasło jest nieprawidłowe'
                    }
                }
            }
        )
    };
};
