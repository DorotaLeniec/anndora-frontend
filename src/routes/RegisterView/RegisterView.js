import React from 'react';
import {Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {registerAction} from "./actions";


const mapStateToProps = state => ({
    token: state.currentUser.token,
    success: state.registerInfo.success
});

const mapDispatchToProps = dispatch => ({
    register: (username, password,sex) => dispatch(registerAction(username,password,sex))
});


class Register extends React.Component {
    constructor() {
        super();

        this.handleSubmit = event => {
            event.preventDefault();
            const username = document.getElementById('reUsername').value;
            const password = document.getElementById('rePassword').value;
            const password2 = document.getElementById('rePassword2').value;
            const sex = document.getElementById('sex').value;
            console.log("sex " + sex)

            let errorMessage = document.getElementById('register-error');
            if(password === password2){
                errorMessage.innerText = "";
                return this.props.register(username,password,sex)
            } else {
                errorMessage.innerText = "Hasła są niezgodne"
            }
        };

        this.state = {

        }
    }

    render() {
        return (

            <Col xs={6} xsOffset={3} className="registerView">
                <style>
                    @import url('https://fonts.googleapis.com/css?family=Saira');
                </style>
                    <div>
                        <h1 className="main-heading">Rejestracja</h1>
                        <br/>
                        <br/>
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" placeholder="Nazwa użytkownika" id="reUsername"/><br/><br/>
                            <input type="password" placeholder="Hasło" id="rePassword"/><br/><br/>
                            <input type="password" placeholder="Hasło" id="rePassword2"/><br/><br/>
                            <select id="sex">
                                <option value="MALE">Płeć</option>
                                <option value="FEMALE">Kobieta</option>
                                <option value="MALE">Mężczyzna</option>
                            </select>

                            <br/>
                            <Button
                                bsStyle="success"
                                type="submit"
                            >
                                Załóż konto
                            </Button>
                            <br/>
                            <br/>
                            <p id="register-error"> </p>
                            <div id="register-box">
                                <h4 id="register-info">Rejestracja przebiegła pomyślnie</h4>
                                <Link to="/logowanie">
                                    <Button bsStyle="success">
                                        Zaloguj się
                                    </Button>
                                </Link>
                            </div>

                        </form>
                    </div>

            </Col>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)