


const initialState = {
   success:false
}


export default (state=initialState, action ) => {
    switch (action.type){
        case 'REGISTER_BEGIN':
            return {
                ...state,
                pending:true,
                username:action.username,
                password:action.password
            };
        case 'REGISTER_SUCCESS':
            return {
                ...state,
                success:true
            };
        default:
            return state
    }
}