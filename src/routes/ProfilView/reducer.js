const initialState = {
    pending: false,
    user: null
};


export default (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_DATA_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'FETCH_DATA_SUCCESS':
            return {
                ...state,
                pending: false,
                user:action.user
            };
        default:
            return state
    }
}