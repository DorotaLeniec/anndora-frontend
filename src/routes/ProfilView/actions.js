export const fetchUserData = (token) => {
    return dispatch => {
        dispatch({type: "FETCH_DATA_BEGIN"});
        console.log("zaczynamy FETCH")
        fetch('http://localhost:8083/user/show', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key':token

            }
        }).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    response.json().then(
                        response => dispatch({type: 'FETCH_DATA_SUCCESS', user:response})

                    )
                }
                else {
                    console.log('Blad '+ response)
                }
            }
        )

    }
};