import React from 'react';
import {Link} from 'react-router-dom';
import {Col, Button} from 'react-bootstrap';
import {fetchUserData} from './actions'
import {connect} from 'react-redux';


const mapStateToProps = state => ({
    token: state.currentUser.token,
    user: state.userData.user
});

const mapDispatchToProps = dispatch => ({
    fetchData: (token) => dispatch(fetchUserData(token)),
});


class Profile extends React.Component {

    componentWillMount() {
        this.props.fetchData(this.props.token);
    }

    render() {
        return (
            <div>
                {this.props.token !== null ? (
                    this.props.user !== null ? (
                        <Col xs={6} xsOffset={3}>
                            <style>
                                @import url('https://fonts.googleapis.com/css?family=Saira');
                            </style>
                            <h1 className="main-heading">Witaj {this.props.user.username}! </h1>
                            <h2 className="main-heading-small">Twoje dane </h2>

                            <table className="table table-bordered">
                                <tbody>

                                <tr className="profile-row">
                                    <th className="profile-col">Nazwa użytkownika :</th>
                                    <th className="profile-col">{this.props.user.username}</th>
                                </tr>

                                <tr className="profile-row">
                                    <th className="profile-col">Płeć :</th>
                                    <th className="profile-col">{this.props.user.sex === 'MALE' ? 'Mezczyzna' : 'Kobieta'}</th>

                                </tr>

                                <tr className="profile-row">
                                    <th className="profile-col">Języki programowania :</th>
                                    <th className="profile-col">{this.props.user.languagesList.map(lan => lan + ' , ')}</th>
                                </tr>

                                </tbody>
                            </table>

                            <Link to={'/'}>
                                <Button
                                    bsStyle="success">
                                    Powrót do strony logowania!
                                </Button>
                            </Link>
                            <Link to={'/users'}>
                                <Button
                                    bsStyle="success">
                                    Pokaż użytkowników
                                </Button>
                            </Link>


                        </Col>
                    ) : "Loading"


                ) : <div className="no-access-box">
                    <p className="no-access-info">Zaloguj się aby wyświetlić swoj profil</p>
                </div>
                }

            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)