import React from 'react';
import {Col, Button, Table} from 'react-bootstrap';
import {connect} from 'react-redux';
import {getNewMessages, getSentMessages, deleteMessage} from "./actions";
import {Link} from 'react-router-dom';


const mapStateToProps = state => ({
    token: state.currentUser.token,
    newMessages: state.messageInfo.newMessages,
    sentMessages: state.messageInfo.sentMessages

});

const mapDispatchToProps = dispatch => ({
    getNew: (token) => dispatch(getNewMessages(token)),
    getSent: (token) => dispatch(getSentMessages(token)),
    deleteMessage: (token,id) => dispatch(deleteMessage(token,id)),
});

class MessageBoxView extends React.Component {
    constructor() {
        super();

        this.state = {
            messageType: 'O'
        }
        this.getNewMessages = event => {
            console.log("odebrane");
            this.setState({
                messageType: 'O'
            });
            // return this.props.getNew(this.props.token)
        };
        this.getSentMessages = event => {
            console.log("wysłane");
            this.setState({
                messageType: 'W'
            });
            // return this.props.getSent(this.props.token)
        };

    }

    componentWillMount() {
        this.props.getNew(this.props.token);
        this.props.getSent(this.props.token);
    }

    render() {
        return (

            <Col xs={6} xsOffset={3} className="homeView">
                <style>
                    @import url('https://fonts.googleapis.com/css?family=Saira');
                </style>
                <h1 className="main-heading">Skrzynka pocztowa</h1>
                {this.props.token !== null ? (
                    <div className="inbox-table">

                        {this.state.messageType === 'O' ? (
                            <Table bordered condensed hover>
                                <thead>
                                <tr>
                                    <th className="from-col">Od:</th>
                                    <th>
                                        <Button bsStyle="primary"
                                                bsSize="small"
                                                onClick={this.getNewMessages}>
                                            Odebrane
                                        </Button>
                                        <Button bsStyle="warning"
                                                bsSize="small"
                                                onClick={this.getSentMessages}>
                                            Wysłane
                                        </Button>
                                        <Button bsStyle="info"
                                                bsSize="small"
                                                onClick={() => this.props.history.push("/send")}>
                                            Nowa wiadomość
                                        </Button>

                                    </th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.props.newMessages !== null && this.props.newMessages.length !== 0 ? (
                                    this.props.newMessages.map(message => (
                                        <tr key={message.id}>
                                            <th className="from-col" key={message.id}>{message.senderName}:</th>
                                            <th key={message.id} className="short-message"
                                                onClick={() => this.props.history.push("/message/" + message.id)}>
                                                {message.shortMessage.substring(0, 5) + "..."}
                                            </th>
                                            <th>
                                                <Button bsStyle="danger"
                                                        bsSize="xsmall"
                                                        onClick={() => this.props.deleteMessage(this.props.token,message.id)}
                                                > Usuń
                                                </Button>
                                            </th>
                                        </tr>
                                    ))
                                ) : <p className="no-msg-info">Brak wiadomości</p>}
                                </tbody>
                            </Table>
                        ) : ''}

                        {this.state.messageType === 'W' ? (
                            <Table bordered condensed hover>
                                <thead>
                                <tr>
                                    <th className="from-col">Do:</th>
                                    <th>
                                        <Button bsStyle="primary"
                                                bsSize="small"
                                                onClick={this.getNewMessages}>
                                            Odebrane
                                        </Button>
                                        <Button bsStyle="warning"
                                                bsSize="small"
                                                onClick={this.getSentMessages}>
                                            Wysłane
                                        </Button>
                                        <Button bsStyle="info"
                                                bsSize="small"
                                                onClick={() => this.props.history.push("/send")}>
                                            Nowa wiadomość
                                        </Button>
                                        <th> </th>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.props.sentMessages !== null && this.props.sentMessages.length !== 0 ? (
                                    this.props.sentMessages.map(message => (
                                        <tr key={message.id}>
                                            <th className="from-col" key={message.id}>{message.receiverName}:</th>
                                            <th key={message.id}
                                                onClick={() => console.log("Id wysłanej " + message.id)}>
                                                {message.content}
                                            </th>
                                            <th>
                                            <Button bsStyle="danger"
                                                    bsSize="xsmall"
                                                    onClick={() => this.props.deleteMessage(this.props.token,message.id)}
                                            > Usuń
                                            </Button>
                                            </th>
                                        </tr>
                                    ))
                                ) : <p className="no-msg-info">Brak wiadomości</p>}
                                </tbody>
                            </Table>
                        ) : ''}
                    </div>
                ) : (
                    <div className="no-access-box">
                        <p className="no-access-info">Zaloguj się aby przeglądać wiadomości</p>
                    </div>
                )

                }

            </Col>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageBoxView)