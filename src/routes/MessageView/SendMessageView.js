import React from 'react';
import {Link} from 'react-router-dom';
import {Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {sendMessage} from "./actions";
import {fetchAllUsers} from "../UsersView/actions";


const mapStateToProps = state => ({
    pending: state.messageInfo.pending,
    error: state.messageInfo.error,
    token: state.currentUser.token,
    isSend: state.messageInfo.isSend,
    allUsers: state.users.allUsers
});

const mapDispatchToProps = dispatch => ({
    send: (receiver, message, token) => dispatch(sendMessage(receiver, message, token)),
    refresh: () => dispatch({type:"REFRESH"}),
    getAllUsers: (token) => dispatch(fetchAllUsers(token))
});


class MessageView extends React.Component {
    constructor() {
        super();

        this.handleSubmit = event => {
            event.preventDefault();
            const receiver = document.getElementById('receiver').value;
            const message = document.getElementById('message').value;
            return this.props.send(receiver, message, this.props.token, false)
        };

        this.refresh = () => {
            this.props.refresh();
            this.props.history.push("/send/success")
        }

        this.state = {
            token: null
        }
    }
    componentWillMount() {
        this.props.getAllUsers(this.props.token)
    }

    render() {
        return (

            <Col xs={6} xsOffset={3} className="homeView">
                {this.props.token !== null ? (
                    <div>
                        <style>
                            @import url('https://fonts.googleapis.com/css?family=Saira');
                        </style>
                        <h1 className="main-heading">Wyślij wiadomość</h1>
                        <form onSubmit={this.handleSubmit}>
                            <span className="full-message-from">Wyślij do:  </span>
                            {this.props.allUsers !== null ? (
                                <select id="receiver">
                                    <option value="Wybierz">--Wybierz--</option>
                                    {this.props.allUsers.map(user => (
                                        <option value={user.username}>{user.username} {user.logged ? '(active)' : ''} </option>
                                    ))}
                                </select>
                            ) : <input type="text" id='receiver'/> }

                            <br/>
                            <br/>
                            <textarea id="message" placeholder="treść wiadomości"/>
                            <br/>
                            <br/>
                            <Link to={'/inbox'}>
                                <Button
                                    bsStyle="success"
                                >
                                    Wróć
                                </Button>
                            </Link>
                            <Button
                                type="submit"
                                bsStyle="success"
                                onClick={() => (
                                    console.log("wysylanie wiadomosci")
                                )}
                            >
                                Wyślij wiadomość
                            </Button>
                            {this.props.isSend? (
                                this.refresh()
                                ) : ''}
                            {this.state.error === null ? '' :  <p id="message-error"> </p>}

                        </form>
                    </div>
                ) : (
                    <div className="no-access-box">
                        <p className="no-access-info">Zaloguj się aby przeglądać wiadomości</p>
                    </div>
                )

                }

            </Col>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageView)