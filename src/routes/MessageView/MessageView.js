import React from 'react'
import {connect} from 'react-redux';
import {getFullMessage} from "./actions";
import {Link} from 'react-router-dom'
import {Button, Col, FormGroup, FormControl} from 'react-bootstrap';
import {sendMessage} from "./actions";


const mapStateToProps = state => ({
    token: state.currentUser.token,
    message: state.messageInfo.fullMessage,
    isSend: state.messageInfo.isSend
});

const mapDispatchToProps = dispatch => ({
    getFull: (token, id) => dispatch(getFullMessage(token, id)),
    send: (receiver, message, token) => dispatch(sendMessage(receiver, message, token)),
    refresh: () => dispatch({type:"REFRESH"})
});


class MessageView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: false
        };
        this.refresh = () => {
            this.props.refresh();
            this.props.history.push("/send/success")
        }
    }

    componentWillMount() {
        this.props.getFull(this.props.token, this.props.match.params.id)
    }


    render() {
        return (
            <Col xs={6} xsOffset={3} className="homeView">
                <style>
                    @import url('https://fonts.googleapis.com/css?family=Saira');
                </style>
                {this.props.message !== null ? (
                    <div>
                        <h1 className="main-heading">Szczegóły</h1>
                        <div className="full-message">
                            <h3 className="full-message-from"> Wiadomość od {this.props.message.senderName} :</h3>
                            <h2 className="full-message-content"> {this.props.message.content}</h2>
                        </div>
                        <Link to="/inbox">
                            <Button bsStyle={"success"}>
                                Wróć
                            </Button>
                        </Link>
                        <Button onClick={() => this.setState({answer: true})}>
                            Odpowiedź
                        </Button>

                        {this.state.answer ? (
                            <div>
                                <br/>
                                <br/>

                                <textarea id="message" placeholder="Odpowiedź..."/>
                                <input type="text" id="to" hidden value={this.props.message.senderName}/>
                                <br/>
                                <Button bsStyle="danger"
                                        onClick={() => {
                                    const to = document.getElementById('to').value;
                                    const message = document.getElementById('message').value;
                                    return this.props.send(to, message, this.props.token, true)
                                }}>
                                    Wyślij
                                </Button>
                                {this.props.isSend? (
                                    this.refresh()
                                ) : ''}
                            </div>
                        ) : ''}
                    </div>
                ) : ''}

            </Col>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageView)