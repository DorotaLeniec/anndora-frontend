import React from 'react';
import {Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default () => (
    <Col  xs={6} xsOffset={3} className="homeView">
        <style>
            @import url('https://fonts.googleapis.com/css?family=Saira');
        </style>
        <h2 className="main-heading-small">Wiadomość wysłana pomyślnie</h2>
        <Link to="/inbox">
        <Button bsStyle="warning">
            Wróć do skrzynki
        </Button>
        </Link>

    </Col>
);