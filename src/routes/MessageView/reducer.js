

const initialState = {
    pending:false,
    error:null,
    isSend:false,
    sentMessages:null,
    newMessages:null,
    fullMessage:null
}


export default (state=initialState, action ) => {
    switch (action.type){
        case 'SENDING_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'SENDING_SUCCESS':
            return {
                ...state,
                pending:false,
                isSend:true
            };
        case 'SENDING_FAIL':
            return {
                ...state,
                error:action.error
            };
        case 'GET_NEW_MSG_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'GET_NEW_MSG_SUCCESS':
            return {
                ...state,
                newMessages:action.messages,
                pending:false
            };
        case 'GET_NEW_MSG_FAIL':
            return {
                ...state,
                error:action.error
            };
        case 'GET_SENT_MSG_FAIL':
            return {
                ...state,
                error:action.error
            };
        case 'GET_SENT_MSG_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'GET_SENT_MSG_SUCCESS':
            return {
                ...state,
                pending:false,
                sentMessages:action.messages
            };
        case ' GET_FULL_MSG_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'GET_FULL_MSG_SUCCESS':
            return {
                ...state,
                pending:false,
                fullMessage:action.fullMessage
            };
        case 'REFRESH':
            return {
                ...state,
                isSend:false
            };
        case 'DELETE_MESSAGE_SUCCESS':
            return {
                ...state,
                pending:false,
            };
        case 'DELETE_MESSAGE_BEGIN':
            return {
                ...state,
                pending:true
            };

        default:
            return state
    }
}