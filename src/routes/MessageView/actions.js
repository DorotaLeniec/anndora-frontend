export const sendMessage = (receiver, message, token, isAnswer) => {
    return (dispatch) => {
        dispatch({type: 'SENDING_BEGIN'});

        let errorMessage = document.getElementById('message-error');
        fetch('http://localhost:8083/message/new', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key': token

            },
            body: JSON.stringify({
                'content': message,
                'receiverName': receiver
            })
        }).then(
            response => {
                if (response.status === 200) {
                    dispatch({type: 'SENDING_SUCCESS'});
                    if(     isAnswer){
                        document.getElementById('receiver').value = '';
                        document.getElementById('message').value = '';
                        errorMessage.innerText = "Wiadomość została wysłana pomyślnie."
                    }

                }
                else {
                    const errorStatus = response.status;
                    dispatch({type: 'SENDING_FAIL', error: errorStatus});
                    console.log(errorStatus)
                    if(errorStatus === 400) {
                        if(!isAnswer){
                            errorMessage.innerText = 'Pole treść lub adresat jest niepoprawne';
                        }
                    } else if(errorStatus === 404) {
                        if(!isAnswer){
                            errorMessage.innerText = 'Taki użytkownik nie istnieje.'

                        }
                    }

                }
            }
        )
    };
};

export const getNewMessages = (token) => {
    return (dispatch) => {
        dispatch({type: 'GET_NEW_MSG_BEGIN'});

        fetch('http://localhost:8083/messages/received', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key': token

            }}).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    response.json().then(
                        response => dispatch({type: 'GET_NEW_MSG_SUCCESS', messages: response})
                    )
                }
                else {
                    let errorMessage = document.getElementById('login-error');
                    if (responseStatus === 401) {
                        errorMessage.innerText = 'Błędne hasło'
                    } else if (responseStatus === 404) {
                        errorMessage.innerText = 'Podany użytkownik lub hasło jest nieprawidłowe'
                    }
                }
            }
        )
    };
};
export const getSentMessages = (token) => {
    return (dispatch) => {
        dispatch({type: 'GET_SENT_MSG_BEGIN'});

        fetch('http://localhost:8083/messages/sent', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key': token
            }}).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    response.json().then(
                        response => dispatch({type: 'GET_SENT_MSG_SUCCESS', messages: response})
                    )
                }
                else {
                    let errorMessage = document.getElementById('login-error');
                    if (responseStatus === 401) {
                        errorMessage.innerText = 'Błędne hasło'
                    } else if (responseStatus === 404) {
                        errorMessage.innerText = 'Podany użytkownik lub hasło jest nieprawidłowe'
                    }
                }
            }
        )
    };
};

export const getFullMessage = (token,id) => {
    return (dispatch) => {
            dispatch({type: 'GET_FULL_MSG_BEGIN'});

        fetch('http://localhost:8083/message/{id}?id=' + id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key': token
            }}).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    response.json().then(
                        response => dispatch({type: 'GET_FULL_MSG_SUCCESS', fullMessage: response})
                    )
                }
                else {
                    let errorMessage = document.getElementById('login-error');
                    if (responseStatus === 401) {
                        errorMessage.innerText = 'Błędne hasło'
                    } else if (responseStatus === 404) {
                        errorMessage.innerText = 'Podany użytkownik lub hasło jest nieprawidłowe'
                    }
                }
            }
        )
    };
};
export const deleteMessage = (token,id) => {
    return (dispatch) => {
        dispatch({type: 'DELETE_MESSAGE_BEGIN'});

        fetch('http://localhost:8083/message/{id}?id=' + id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'api_key': token
            }}).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    dispatch({type:'DELETE_MESSAGE_SUCCESS'});
                   dispatch(getNewMessages(token));
                   dispatch(getSentMessages(token))
                }
            }
        )
    };
};