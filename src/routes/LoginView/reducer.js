

const initialState = {
    token:null,
    pending:false,
    username:null,
    password:null,
    error:false
}


export default (state=initialState, action ) => {
    switch (action.type){
        case 'LOGIN_BEGIN':
            return {
                ...state,
                pending:true,
                username:action.username,
                password:action.password
            };
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                pending:false,
                token:action.token
            };
        case 'LOGIN_FAIL':
            return {
                ...state,
                error:true
            };
        case 'LOGOUT_BEGIN' :
        return {
            ...state
        };
        case 'LOGOUT_SUCCESS':
            return {
                ...state,
                token:null
            };
        default:
            return state
    }
}