import React from 'react';
import {Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {loginAction} from './loginAction.js'



const mapStateToProps = state => ({
    token: state.currentUser.token
});

const mapDispatchToProps = dispatch => ({
    login: (username, password) => dispatch(loginAction(username, password))
});


class Login extends React.Component {
    constructor() {
        super();

        this.handleSubmit = event => {
            event.preventDefault();
            const username = document.getElementById('loginUsername').value;
            const password = document.getElementById('loginPassword').value;
            return this.props.login(username, password)
        };

        this.state = {
            token: null
        }
    }

    render() {
        return (

            <div>
                <style>
                    @import url('https://fonts.googleapis.com/css?family=Saira');
                </style>
                {this.props.token === null ?
                    (
                        <Col xs={6} xsOffset={3} className="loginView">
                            <div>
                                <h1 className="main-heading">Zaloguj się</h1>
                                <br/>
                                <br/>
                                <form onSubmit={this.handleSubmit}>
                                    <input type="text" placeholder="Nazwa użytkownika" id='loginUsername'/>
                                    <br/>
                                    <br/>
                                    <input type="password" placeholder="Hasło" id='loginPassword'/>
                                    <br/>
                                    <br/>
                                    <br/>

                                    <Button
                                        bsStyle="success"
                                        type="submit"
                                    >
                                        Zaloguj się
                                    </Button>
                                </form>
                                <br/>
                                <p id="login-error"> </p>
                            </div>
                        </Col>
                    ) :

                    this.props.history.push('/profil')
                }
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)