export const loginAction = (username, password) => {
    return (dispatch) => {
        dispatch({type: 'LOGIN_BEGIN'});

        fetch('http://localhost:8083/user/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            },
            body: JSON.stringify({
                'username': username,
                'password': password
            })
        }).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                    response.json().then(
                        response => dispatch({type: 'LOGIN_SUCCESS', token: response.token})
                    )
                }
                else {
                    let errorMessage = document.getElementById('login-error');
                    if (responseStatus === 401) {
                        errorMessage.innerText = 'Błędne hasło'
                    } else if (responseStatus === 404) {
                        errorMessage.innerText = 'Podany użytkownik lub hasło jest nieprawidłowe'
                    }
                }
            }
        )
    };
};

export const logout = (token) => {
    return (dispatch) => {
        dispatch({type: 'LOGIN_BEGIN'});

        fetch('http://localhost:8083/user/logout', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key':token

            }}).then(
            response => {
                const responseStatus = response.status;
                if (responseStatus === 200) {
                        dispatch({type: 'LOGOUT_SUCCESS'})
                }
                else {
                    let errorMessage = document.getElementById('login-error');
                    if (responseStatus === 401) {
                    } else if (responseStatus === 404) {

                    }

                }
            }
        )
    };
};