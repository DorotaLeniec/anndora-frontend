import { compose, createStore, combineReducers, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'


import currentUserReducer from './LoginView/reducer';
import messageReducer from './MessageView/reducer';
import inboxReducer from './InboxView/reducer';
import registerReducer from './RegisterView/reducer';
import userReducer from './ProfilView/reducer';
import usersReducer from './UsersView/reducer'


const reducer = combineReducers({
   currentUser: currentUserReducer,
    messageInfo: messageReducer,
    inboxInfo: inboxReducer,
    registerInfo: registerReducer,
    userData: userReducer,
    users: usersReducer

});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(
    applyMiddleware(
        thunkMiddleware
    )
);

const store = createStore(reducer, enhancer);

export default store