import React from 'react';
import {Link} from 'react-router-dom';
import {Col, Button} from 'react-bootstrap';

import {default as Heart} from './Heart'
import './HomeView.css';


export default () => (
    <Col  xs={6} xsOffset={3} className="homeView">
        <style>
            @import url('https://fonts.googleapis.com/css?family=Saira');
        </style>
        <h1 className="main-heading">HelloLove </h1>
        <Heart/>
        <h1 className="main-heading-small">Zaprogramuj swoją miłość!</h1>
        <br/>
        <br/>
        <Link to={'/logowanie'}>
            <Button
                bsStyle="success"
            >
                Zaloguj się
            </Button>
        </Link>


        <Link to={'/rejestracja'}>
            <Button
                bsStyle="success"
            >
                Załóż konto!
            </Button>
        </Link>

    </Col>
);