import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import './Navigation.css'
import {logout} from "../LoginView/loginAction";

import TiMessage from 'react-icons/lib/ti/messages'


const mapStateToProps = state => ({
    token: state.currentUser.token
});

const mapDispatchToProps = dispatch => ({
    logout: (token) => dispatch(logout(token))
});

class Navigation extends React.Component {

    render() {
        return (
            <div>
                <ul className="navigation-list">
                    <Link to="/">
                        <li className="navigation-element home-button">Home</li>
                    </Link>
                    {this.props.token === null ? (
                        <Link to="/logowanie">
                            <li className="navigation-element">Logowanie</li>
                        </Link>
                    ) : ""}
                    {this.props.token === null ? (
                        <Link to="/rejestracja">
                            <li className="navigation-element">Rejestracja</li>
                        </Link>
                    ) : ""}

                    {this.props.token !== null ? (
                        <Link to="/profil">
                            <li className="navigation-element">Profil</li>
                        </Link>
                    ) : ""
                    }
                    {this.props.token !== null ? (
                        <Link to="/inbox">
                            <li className="navigation-element"><TiMessage size={20}/></li>
                        </Link>
                    ) : ""
                    }
                    {this.props.token !== null ? (
                        <Link to="/">
                            <li className="navigation-element" onClick={() => this.props.logout(this.props.token)}>Wyloguj</li>
                        </Link>
                    ) : ""
                    }
                    {this.props.token ==='admin_admin' ? (
                        <Link to="/admin">
                            <li className="navigation-element" >Władza</li>
                        </Link>
                    ) : ""
                    }

                </ul>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation)