import React from 'react';
import {Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux'


const mapStateToProps = state => ({
    token: state.currentUser.token,
    messagesSent: state.inboxInfo.messagesSent
});

const mapDispatchToProps = dispatch => ({
    get: (token) => dispatch(getMessages(token))
});

const getMessages = (token) => {
    return (dispatch) => {
        dispatch({type: 'GET_MESSAGE_BEGIN'});

        fetch('http://localhost:8083/messages/sent', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'api_key': token

            }
        }).then(
            response => {
                if (response.status === 200) {
                    response.json().then(
                        response => dispatch({type: 'GET_MESSAGE_SUCCESS', messagesSent: response})
                    )
                }
                else {
                    response.json.then(
                        response => {
                            return (
                                dispatch({type: 'GET_MESSAGE_FAIL', error: response.status})
                            )
                        }
                    )
                }
            }
        )
    }
};

class InboxView extends React.Component {
    constructor() {
        super();

        this.handleSubmit = event => {
            event.preventDefault();
        };

        this.state = {
            token: null
        }
    }

    render() {
        return (
            <Col xs={6} xsOffset={3}>
                <h1 className="main-heading">Wiadomości</h1>


                    <Button
                        bsStyle="success"
                        onClick={() => this.props.get(this.props.token)}
                    >
                        Pokaż
                    </Button>
                {
                    this.props.messagesSent === null ? '': (
                        <table>
                            <tbody>


                            {this.props.messagesSent.map(
                            message => (
                                <tr key={message.id}>
                                    <th key={message.id}>{message.content}</th>
                                    <th key={message.id}>{message.receiverName}</th>
                                </tr>
                            )
                        )}
                            </tbody>
                        </table>
                    )

                }

            </Col>
        )

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InboxView)