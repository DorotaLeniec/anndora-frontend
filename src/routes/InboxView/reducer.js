

const initialState = {
    pending:false,
    error:false,
    messagesSent:null
}


export default (state=initialState, action ) => {
    switch (action.type){
        case 'GET_MESSAGE_BEGIN':
            return {
                ...state,
                pending:true
            };
        case 'GET_MESSAGE_SUCCESS':
            return {
                ...state,
                pending:false,
                messagesSent: action.messagesSent
            };
        case 'GET_MESSAGE_FAIL':
            return {
                ...state,
                error:action.error
            };
        default:
            return state
    }
}