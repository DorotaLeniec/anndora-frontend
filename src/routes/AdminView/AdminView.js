import React from 'react';
import {Col, Table,Button} from 'react-bootstrap';
import {connect} from 'react-redux'

import {fetchAllUsers,deleteUser} from "../UsersView/actions";


const mapStateToProps = state => ({
    token: state.currentUser.token,
    allUsers: state.users.allUsers

});

const mapDispatchToProps = dispatch => ({
    getAllUsers: (token) => dispatch(fetchAllUsers(token)),
    deleteUser: (username,token) => dispatch(deleteUser(username,token))

});
class AdminView extends React.Component {
    constructor() {
        super();

        this.state = {
            messageType: 'O'
        }
    }

    componentWillMount() {
        this.props.getAllUsers(this.props.token)
    }

    render() {
        return (
            <Col xs={6} xsOffset={3} className="homeView">
                <style>
                    @import url('https://fonts.googleapis.com/css?family=Saira');
                </style>
                <h2 className="main-heading-small">Witamy w panelu administratora </h2>
                <h3 className="panel-management"> Zarządzanie kontami użytkowników</h3>
                <div className="table-responsive">
                    {this.props.allUsers !== null ? (
                        <div>
                            <Table  bordered condensed hover>
                                <thead>
                                <tr>
                                    <th>Zdjecie</th>
                                    <th>Username</th>
                                    <th>Hasło</th>
                                    <th>Płeć</th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>

                                {this.props.allUsers.map(user => (
                                        <tr>
                                            <th>
                                                <div className="user-photo">{user.username.substring(0,1).toUpperCase()}
                                                </div>
                                            </th>
                                            <th>{user.username}</th>
                                            <th>{user.password}</th>
                                            <th>{user.sex}</th>
                                            <th>
                                                <Button bsStyle="danger"
                                                        bsSize="xsmall"
                                                        onClick={() => this.props.deleteUser(user.username, this.props.token)}
                                                >
                                                    Usuń
                                                </Button>
                                            </th>
                                        </tr>
                                    )
                                )}
                                </tbody>
                            </Table>

                        </div>
                    ):""}

                </div>
            </Col>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminView)